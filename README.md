**Login Provider**
This is a flutter app using a simple Provider with change notifier and custom animations for the splash screen.

**Splash Screen**

<img src="screenshots/splashScreen.png" height = "400">


**Animations**

<img src="screenshots/animation.png" height = "400">
<img src="screenshots/animation1.png" height = "400">
<img src="screenshots/animation2.png" height = "400">


| **Login Screen** | **Home Screen** |
| ------ | ------ |
| <img src="screenshots/loginScreen.png" height = "400"> | <img src="screenshots/homeScreen.png" height = "400"> |

