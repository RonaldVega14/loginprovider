import 'package:flutter/material.dart';
import 'package:login_provider/animations/fade_animation.dart';
import 'package:login_provider/states/login_states.dart';
import 'package:provider/provider.dart';

class MyLoginPage extends StatefulWidget {
  MyLoginPage({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

  String _email;
  String _password;

  @override
//Esto es para mantener un listener siempre activo.
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

//Metodo para ingresar a la pagina.
  void _login() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();

      performLogin();
    }
  }

  void performLogin() {
    //Utilizo el provider de tipo Login State para mandar a llamar la funcion de login.
    Provider.of<LoginState>(context, listen: false).login();
  }

//------------------------------------------------------------------------------------------------
  TextStyle style =
      TextStyle(fontFamily: 'Montserrat', fontSize: 16.0, color: Colors.black);

  @override
  Widget build(BuildContext context) {
//Creando los campos para ingresar contraseña y Email
    final emailField = TextFormField(
      validator: (val) => val.length < 5 ? 'Invalid Email' : null,
      onSaved: (val) => _email = val,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black),
      decoration: InputDecoration(
          labelText: 'Email',
          contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
    );
//------------------------------------------------------------------------------------------------
    final passwordField = TextFormField(
//ObscureText sirve para ocultar lo que el usuario va a escribir.
      validator: (val) => !(val.length > 6) ? 'Invalid Password' : null,
      onSaved: (val) => _password = val,
      obscureText: true,
      style: TextStyle(
          fontFamily: 'Montserrat', fontSize: 14.0, color: Colors.black),
      decoration: InputDecoration(
//label text es necesario para cumplir con material design guidelines
          labelText: 'Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
    );
//------------------------------------------------------------------------------------------------
//Creando el boton para Login
    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(12.0),
      color: Colors.blue[700],
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
        onPressed: _login,
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
//------------------------------------------------------------------------------------------------
    final singUpButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(12.0),
      color: Colors.blue[900],
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
        onPressed: () {},
        child: Text("Sing Up",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    Widget twoButtons() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: loginButton,
            ),
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: singUpButton,
            ),
          ),
        ],
      );
    }

//-------------------------------------------------------------------------------------------------
//Creando la interfaz grafica
    return Consumer<LoginState>(
      builder: (BuildContext context, LoginState value, Widget child) {
        if (value.isLoading()) {
          return CircularProgressIndicator();
        } else {
          return child;
        }
      },
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          key: scaffoldKey,
          resizeToAvoidBottomPadding: false,
          body: Center(
            child: new Form(
              key: formKey,
              child: Container(
                color: Colors.white,
                child: Padding(
                    padding: const EdgeInsets.all(36.0),
                    child: SingleChildScrollView(
                      padding: const EdgeInsets.all(5.0),
                      reverse: true,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
//Este SizedBox sirve para crear un espacio definido donde poner la imagen o Logo.
                          CustomFadeAnimation(
                            1,
                            SizedBox(
                              child: Image.asset(
                                "assets/RonaldVegaLogo.png",
                                fit: BoxFit.contain,
                                width: 250,
                                height: 250,
                              ),
                            ),
                          ),
//Estos SizedBox sirven para crear espacios en blanco entre un campo y otro.
                          SizedBox(height: 5.0),
//EmailField esta creado arriba al igual que los demas campos.(passwordField y loginButton)
                          CustomFadeAnimation(1.3, emailField),
                          SizedBox(height: 25.0),
                          CustomFadeAnimation(1.3, passwordField),
                          SizedBox(height: 35.0),
                          CustomFadeAnimation(1.6, twoButtons()),
                          SizedBox(height: 15.0),
                        ],
                      ),
                    )),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
