import 'package:flutter/material.dart';
import 'package:login_provider/utils/my_flutter_app_icons.dart' as CustomIcons;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(
        fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black);
    TextStyle subStyle = TextStyle(
        fontFamily: 'Montserrat', fontSize: 15.0, color: Colors.black);

    Widget _row(String text1, String text2, double height) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child:
                Text(text1, style: subStyle.copyWith(color: Colors.blue[900])),
            flex: 4,
            fit: FlexFit.loose,
          ),
          Flexible(
            child:
                Text(text2, style: subStyle.copyWith(color: Colors.blue[900])),
            flex: 6,
            fit: FlexFit.loose,
          ),
        ],
      );
    }

    Widget _column(String titulo, String total, double height) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Flexible(
              child: Text(titulo,
                  style: style.copyWith(
                      color: Colors.blue[900], fontWeight: FontWeight.bold)),
              flex: 1,
              fit: FlexFit.loose),
          Flexible(
              child: _row('COMPLETELY ', total, height),
              flex: 1,
              fit: FlexFit.loose),
        ],
      );
    }

    Widget macros(double height, double width, BuildContext context,
        String titulo, String total) {
      return Container(
        width: width,
        height: height,
        padding: const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
        child: InkWell(
          onTap: () {},
          child: Material(
            color: Colors.white.withOpacity(1),
            elevation: 12.0,
            shadowColor: Colors.black,
            borderRadius: BorderRadius.circular(24.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    child: _column(
                      titulo,
                      total,
                      height,
                    ),
                    flex: 9,
                    fit: FlexFit.loose),

                //linea y flecha
                Flexible(
                    child: Container(
                      height: height * 0.80,
                      width: 1.0,
                      color: Colors.blue[900].withOpacity(0.6),
                    ),
                    flex: 1,
                    fit: FlexFit.loose),
                Flexible(
                    child: Icon(
                      CustomIcons.MyFlutterApp.chevron_right,
                      color: Colors.blue[900],
                    ),
                    flex: 1,
                    fit: FlexFit.loose),
              ],
            ),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            SizedBox(
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Image.asset(
                  "assets/RonaldVegaLogo.png",
                  fit: BoxFit.contain,
                  height: 150.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Text(
                'HOME SCREEN',
                style: style.copyWith(
                    color: Colors.blue[900], fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 100),
            macros(
                MediaQuery.of(context).size.height * 0.2,
                MediaQuery.of(context).size.width * 0.8,
                context,
                'LET\'S CREATE SOMETHING',
                'AWESOME')
          ],
        ),
      ),
    );
  }
}
