import 'package:flutter/material.dart';
import 'package:login_provider/pages/home_page.dart';
import 'package:login_provider/pages/login_page.dart';
import 'package:login_provider/pages/splash_page.dart';
import 'package:login_provider/states/login_states.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginState>(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: {
          '/': (BuildContext context) {
            var state = Provider.of<LoginState>(context);
            if (state.onSplashScreen()) {
              return MySplashPage();
            } else {
              if (state.isLoggedIn()) {
                return MyHomePage();
              } else {
                return MyLoginPage();
              }
            }
          }
        },
      ),
      create: (BuildContext context) => LoginState(),
    );
  }
}
