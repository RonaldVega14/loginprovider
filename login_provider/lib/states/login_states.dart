import 'package:flutter/cupertino.dart';

class LoginState with ChangeNotifier {
  bool _onSplashScreen = true;

  bool onSplashScreen() => _onSplashScreen;

  bool _loggedIn = false;

  bool isLoggedIn() => _loggedIn;

  bool _loading = false;

  bool isLoading() => _loading;

  void splashScreenOver() {
    _onSplashScreen = false;
    notifyListeners();
  }

  void login() {
    _loading = true;
    notifyListeners();
    _loggedIn = true;
    _loading = false;
    notifyListeners();
  }

  void logout() {
    _loggedIn = false;
    notifyListeners();
  }
}
